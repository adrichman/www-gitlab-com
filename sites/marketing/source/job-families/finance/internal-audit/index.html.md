---
layout: job_family_page
title: "Internal Audit"
---

## Internal Auditor (Intermediate)

The intermediate Internal Auditor is responsible for performing individual internal audit projects with supervision, as part of the total internal audit plan. This responsibility includes performing initial internal audit procedures and assisting in preparing internal audit reports reflecting the results of the work performed. Additionally, the internal auditor performs follow-up on the status of outstanding internal audit issues, as directed.

### Job Grade

The Intermediate Internal Auditor is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

- Assist in the preparation of audit test steps designed to evaluate the adequacy and effectiveness of internal controls.
- Assist in documenting GitLab’s financial/audit processes.
- Assist in creating GitLab issues to collect preliminary data, conduct analyses and perform transactional and control based testing.
- Assist in the identification and documentation of weaknesses in control design and effectiveness based on analyses performed.
- Assist with follow-ups on key management actions from prior audit reports determining if required action was taken.

### Requirements

- At least 2-4  years of experience in auditing or a related field
- Bachelor’s degree in Accounting, Business Administration, or a related field
- Related professional designation (CPA, CISA, CIA) (preferred)
- Understanding of internal control concepts and experience in applying them to plan, perform and report on the evaluation of various business processes/areas/functions.
- Ability to work on complex tasks with the  required direction and guidance
- Strong verbal and written communication skills
- Understanding of the technical aspects of accounting and financial reporting.

### Performance Indicators

- [Percentage of Desktop procedures documented](https://about.gitlab.com/handbook/internal-audit/#internal-audit-performance-measures)
- [Percentage of controls tested](https://about.gitlab.com/handbook/internal-audit/#internal-audit-performance-measures)

### Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our team page.

- Selected candidates will be invited to schedule a screening call with our Global Recruiters
- Next, candidates will be invited to schedule a first interview with our Senior Internal Audit Manager
- Candidates will then be invited to schedule a second round of interview with Principal Accounting Officer
- Candidates will then be invited to schedule a final round of interviews with our CFO.

Additional details about our process can be found on our [hiring page](/handbook/hiring/interviewing).

The Internal Auditor is responsible for planning and execution of audits in accordance with the annual audit plan, monitoring the status of implementation of audit recommendations, and helping with any advisory or ad hoc work as desired by management. The Internal Auditor brings a systematic, disciplined approach to the assessment and improvement of governance, risk management, and control process.

## Senior Internal Auditor

### Job Grade

The Intermediate Senior Internal Auditor is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities
- Contributes to the development of the annual audit plan for business units in the assigned audit portfolio
- Develops detailed and thorough risk assessment for each audit engagement
- Owns each assigned audit from the initial planning to completion
- Effectively communicates the objectives, scope and timelines of the audit to the auditees and relevant stakeholders at the start of the engagement
- Documents detailed working papers and ensures that audit conclusions are supported by sufficient and relevant audit evidence
- Effectively communicates audit findings to the auditee
- Drafts audit reports including value-added observations
- Follows up and monitors the progress of the implementation of recommendations
- Develops and maintains effective working relationships with business and corporate functions
- Supports the alignment of activities between the control functions (i.e. Compliance, Finance, External Auditors, etc.) to improve communications and efficiency of audit and risk management activities
- Assists with quarterly and annual reporting to the Audit Committee, Board of Directors and Senior Management
- Performs advisory engagements, investigations and special projects as assigned
- Continually improves skills and competencies required for the position
- Identifies opportunities for internal audit to provide value added services to the organization
- Maintains an up-to-date knowledge of the standards and guidance included in the International Professional Practice Framework (IPPF) developed by the IIA; stays current with evolving knowledge in the field of Internal Auditing; maintains compliance with IIA standards and its Code of Ethics

### Requirements

- Minimum five (5) years experience conducting internal audits, including minimum two (2) years of experience in Information Technology industry in internal / external audit preferred.
- Audit experience at a public accounting firm is considered as an asset
- Knowledge of audit methodologies and frameworks and related governance concepts, tools, techniques, and best practices
- Undergraduate degree in Business, Accounting or Finance
- Chartered Accountant/ CPA /CIA certification preferred
- Certified Internal Auditor (CIA), Certified Information Systems Auditor (CISA), or Certified Fraud Examiner (CFE) designations are considered an asset
- Excellent written and oral communication skills
- Have an understanding of enterprise risk management framework such as COSO enterprise risk management framework
- Ability to communicate information in an understandable form to the right parts of the organization
- Ability to work effectively in a team environment, both within Internal Audit and across other departments
- Must be able to work in US and Canada timezones, when required
- Ability to use GitLab

## Performance Indicators
- [Percentage of Desktop procedures documented](/handbook/internal-audit/#internal-audit-performance-measures)
- [Percentage of controls tested](/handbook/internal-audit/#internal-audit-performance-measures)
- [Percentage of recommendations implemented](/handbook/internal-audit/#internal-audit-performance-measures)
- [Percentage of audits completed](/handbook/internal-audit/#internal-audit-performance-measures)

### Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our team page.

1. Selected candidates will be invited to schedule a screening call with our Global Recruiters
1. Next, candidates will be invited to schedule a first interview with our Senior Internal Audit Manager
1. Candidates will then be invited to schedule a second round of interview with Principal Accounting Officer
1. Candidates will then be invited to schedule a final round of interview with our CFO.

Additional details about our process can be found on our hiring page.

## Manager, Internal Audit

### Job Grade

The Manager, Internal Audit  is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

* Primary responsibility for determining, documenting and implementing the Company’s internal audit policies.
* Document financial processes and perform test of controls for SOX compliance and audits.
* Assist with the execution of the internal audit plan and timely completion of assigned audits.
* Assist with enterprise risk management activities.
* Recommend improvements related to the Company’s key controls.
* Work closely with external auditors.
* Must be able to work collaboratively with the operational accounting team and business functions.
* Responds to inquiries from the CFO, Controller, and company wide managers regarding financial results, special reporting requests and the like.
* Act as a subject matter expert, working with the business partners in accounting and other functions (e.g., legal, corporate development, stock administration) to identify financial risks associated with new or contemplated transactions and resolve complex accounting issues.
* Participate in team planning including setting team goals and priorities, monitoring progress and removing roadblocks.

### Requirements

* Proven work experience in Internal Auditing or similar role.
* Must have at least 5 years experience in a big 4 public accounting firm.
* Certified Public Accountant desirable.
* Strong technical, analytic, and communication skills (both written and verbal).
* In-depth knowledge of SEC filing requirements, experience highly preferred.
* Must have direct knowledge of US GAAP.
* Must have public company experience with Sarbanes Oxley.
* Strong working knowledge of GAAP principles and financial statements.
* Proficient with Google Sheets and Google Workspace.
* Revenue recognition, namely 606, experience highly preferred.
* Detail-oriented, self-directed and able to effectively complete tasks with minimal supervision.
* Ability to balance quality of work with speed of execution.
* Ability to use GitLab.

## Director, Internal Audit

The Director, Internal Audit reports directly to the chairman of the audit committee with a dotted line day to day control and administrative reporting relationship with the principal accounting officer. The Director, Internal Audit will be responsible for preparing and implementing a risk-based audit plan to assess, report on, and recommend improvements to the company’s key operational and finance activities and internal controls. Additionally, the position is responsible for identifying and assisting in documenting existing internal finance and disclosure controls, implementing and documenting new internal controls, and establishing an internal monitoring function to audit the company’s compliance with such internal controls. The position will have a key role in assessing the company’s compliance with the requirements of the Sarbanes-Oxley Act of 2002. The position will be further called on to identify and implement finance department process improvements.  Once documented, ownership for and changing internal control procedures will reside in the owner of the control.

### Job Grade

The Director, Internal Audit is a [grade 10](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

* Drives the reporting package to the Audit Committee.
* Documents GitLab SOX controls, processes, and recommends additional controls where there are control deficiencies.
* Is the subject matter expert on controls with GitLab business partners, audit committee, and C-Suite executives.
* Build and manage a highly functioning, distributed team of direct reports.
* Manages an intern program to bring on new personnel to train and to fit into the accounting/finance functions.
* Works with the Legal and Technical Accounting teams to identify related party companies from inquiries from the Board and C-Suite team.
* Identify, understand, and document processes and procedures surrounding internal controls. Continually monitor and update the assessment of the control environment, keeping abreast of significant control issues, trends and developments.
* Develop test plans and coordinate the performance of management testing of internal controls required by Sarbanes-Oxley.
* Identify and assess the implementation of new controls as necessary.
* Prepare and update a comprehensive risk-based audit plan for evaluating the effectiveness of controls in place to manage significant risk exposures, ensure the integrity and reliability of information and financial reporting, safeguard company assets, and comply with laws and regulations.
* Identify and design anti-fraud criteria and controls.
* Monitor and audit the company’s compliance with established internal controls.
* Establish procedures and plan for conducting internal control audits.
* Report findings to senior management and the company’s audit committee.
* Understand the requirements of the Sarbanes-Oxley Act of 2002 (and any related SEC pronouncements) and assist in maintaining processes and functions to help ensure compliance with such requirements.
* Coordinate activities with external auditors to support their audit and review procedures.
* Participate in disclosure committee meetings.
* Review finance department business processes and suggest ways to improve such processes.
* Other duties, as directed by the Audit Committee and/or the Principal Accounting Officer:
  - Identify and Review Financial Risks within the Company.
  - Develop Audit plans that will be approved by the Audit Committee to review 1-3 Financial Risks audits and report findings first to the area related to these Financial Risks and report final reports to the Audit Committee.
  - Handle other audits or reviews as directed by the Audit Committee, CFO or PAO.

### Requirements

* Previous management experience; ability to contribute to the career development of staff and a culture of teamwork.
* Comprehensive knowledge of auditing practices, procedures, and principles, sufficient to interpret and analyze complex concepts and apply them in innovative ways. Skills and knowledge should include an extensive understanding of financial, operational, market and credit risk. Should have expert knowledge of generally accepted auditing standards in the US.
* Capability and desire to evaluate the effectiveness of management in their stewardship of GitLab’s resources and their compliance with established corporate policy and procedures, including corporate governance, code of conduct standards, and business ethics and conduct policy.
* Creative approaches and solutions necessary to solve complex problems.
* Strong written and verbal communication skills with experience interacting with and presenting to senior management-level personnel.
* The candidate must have excellent interpersonal skills and will serve as a member of the senior management team.
* The candidate should be an energetic, entrepreneurial self-starter capable of self-direction.
* He/she should be driven to deliver quality results on time, with a high degree of integrity, in a highly ethical and professional manner.
* The candidate should be self-reliant and have strong initiative as well as possess solid business judgment.
* He/she must be resourceful and strategic and possess excellent analytical abilities.
* Able to utilize Best Practices on recommendations and audit findings.
* A minimum of ten years of experience in a public accounting firm and or software industry with a heavy emphasis on financial and accounting applications and financial and operational controls.
* Experience in an internal audit function preferred.
* Experience in the software industry highly preferred.
* Bachelor’s degree in accounting or related finance field. Chartered accountant (CA) or certified public accountant (CPA) desirable.
* Certified internal auditor (CIA) or certified information systems auditor (CISA) preferred.
* Ability to use GitLab

## Performance Indicators
* [Percentage of Desktop procedures documented](/handbook/internal-audit/#internal-audit-performance-measures)
* [Percentage of controls tested](/handbook/internal-audit/#internal-audit-performance-measures)
* [Percentage of recommendations implemented](/handbook/internal-audit/#internal-audit-performance-measures)
* [Percentage of audits completed](/handbook/internal-audit/#internal-audit-performance-measures)
*  [New Hire Location Factor < 0.69](/handbook/business-ops/metrics/#new-hire-location-factor--069)
*  Completing tasks and audits timely and efficiently
*  Utilizing Best Practices related to audit findings and recommendations.
*  Approval ratings based on surveys above 80% in the first year, moving to 90% in subsequent years.

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).

- Selected candidates will be invited to schedule a 30 minute screening call with our Global Recruiters
- Next, candidates will be invited to schedule a 45 minute interview with our Controller
- Candidates will then be invited to schedule a 45 minute interview with our CFO
- Finally, candidates will interview with Chairman of the Audit Committee
- Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring/interviewing).

## Career Ladder

The next step in the Internal Audit job family is to move to the [PAO](/job-families/finance/pao-jf/) job family.
