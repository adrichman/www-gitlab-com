---
layout: handbook-page-toc
title: "Identity data"
description: "GitLab country specific data in regard to team members location, gender, ethnicity, race, age etc. View data here!"
canonical_path: "/company/culture/inclusion/identity-data/"
---

#### GitLab Identity Data

Data as of 2021-02-28

##### Country Specific Data

| **Country Information**                     | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Based in APAC                               | 144       | 11.29%          |
| Based in EMEA                               | 353       | 27.66%          |
| Based in LATAM                              | 23        | 1.80%           |
| Based in NORAM                              | 756       | 59.25%          |
| **Total Team Members**                      | **1,276** | **100%**        |

##### Gender Data

| **Gender (All)**                            | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men                                         | 871       | 68.26%          |
| Women                                       | 405       | 31.74%          |
| Other Gender Identities                     | 0         | 0%              |
| **Total Team Members**                      | **1,276** | **100%**        |

| **Gender in Management**                    | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men in Management                           | 152       | 66.96%          |
| Women in Management                         | 75        | 33.04%          |
| Other Gender Management                     | 0         | 0%              |
| **Total Team Members**                      | **227**   | **100%**        |

| **Gender in Leadership**                    | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men in Leadership                           | 75        | 72.82%          |
| Women in Leadership                         | 28        | 27.18%          |
| Other Gender Identities                     | 0         | 0%              |
| **Total Team Members**                      | **103**   | **100%**        |

| **Gender in Engineering**                   | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men in Engineering                          | 438       | 79.49%          |
| Women in Engineering                        | 113       | 20.51%          |
| Other Gender Identities                     | 0         | 0%              |
| **Total Team Members**                      | **551**   | **100%**        |

##### Race/Ethnicity Data

| **Race/Ethnicity (US Only)**                | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 1         | 0.14%           |
| Asian                                       | 53        | 7.52%           |
| Black or African American                   | 20        | 2.84%           |
| Hispanic or Latino                          | 36        | 5.11%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 30        | 4.26%           |
| White                                       | 441       | 62.55%          |
| Unreported                                  | 124       | 17.59%          |
| **Total Team Members**                      | **705**   | **100%**        |

| **Race/Ethnicity in Engineering (US Only)** | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 18        | 9.00%           |
| Black or African American                   | 3         | 1.50%           |
| Hispanic or Latino                          | 5         | 2.50%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 8         | 4.00%           |
| White                                       | 135       | 67.50%          |
| Unreported                                  | 31        | 15.50%          |
| **Total Team Members**                      | **200**   | **100%**        |

| **Race/Ethnicity in Management (US Only)**  | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 10        | 7.19%           |
| Black or African American                   | 3         | 2.16%           |
| Hispanic or Latino                          | 4         | 2.88%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 5         | 3.60%           |
| White                                       | 87        | 62.59%          |
| Unreported                                  | 30        | 21.58%          |
| **Total Team Members**                      | **139**   | **100%**        |

| **Race/Ethnicity in Leadership (US Only)**  | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 12        | 14.63%          |
| Black or African American                   | 0         | 0.00%           |
| Hispanic or Latino                          | 0         | 0.00%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 3         | 3.66%           |
| White                                       | 53        | 64.63%          |
| Unreported                                  | 14        | 17.07%          |
| **Total Team Members**                      | **82**    | **100%**        |

| **Race/Ethnicity (Global)**                 | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 1         | 0.08%           |
| Asian                                       | 134       | 10.50%          |
| Black or African American                   | 33        | 2.59%           |
| Hispanic or Latino                          | 68        | 5.33%           |
| Native Hawaiian or Other Pacific Islander   | 2         | 0.16%           |
| Two or More Races                           | 38        | 2.98%           |
| White                                       | 720       | 56.43%          |
| Unreported                                  | 280       | 21.94%          |
| **Total Team Members**                      | **1,276** | **100%**        |

| **Race/Ethnicity in Engineering (Global)**  | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 64        | 11.62%          |
| Black or African American                   | 11        | 2.00%           |
| Hispanic or Latino                          | 27        | 4.90%           |
| Native Hawaiian or Other Pacific Islander   | 1         | 0.18%           |
| Two or More Races                           | 14        | 2.54%           |
| White                                       | 308       | 55.90%          |
| Unreported                                  | 126       | 22.87%          |
| **Total Team Members**                      | **551**   | **100%**        |

| **Race/Ethnicity in Management (Global)**   | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 17        | 7.49%           |
| Black or African American                   | 3         | 1.32%           |
| Hispanic or Latino                          | 6         | 2.64%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 6         | 2.64%           |
| White                                       | 141       | 62.11%          |
| Unreported                                  | 55        | 23.79%          |
| **Total Team Members**                      | **227**   | **100%**        |

| **Race/Ethnicity in Leadership (Global)**   | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 13        | 12.62%          |
| Black or African American                   | 0         | 0.00%           |
| Hispanic or Latino                          | 1         | 0.97%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 3         | 2.91%           |
| White                                       | 64        | 62.14%          |
| Unreported                                  | 22        | 21.36%          |
| **Total Team Members**                      | **103**   | **100%**        |

##### Age Distribution

| **Age Distribution (Global)**               | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| 18-24                                       | 15        | 1.18%           |
| 25-29                                       | 200       | 15.67%          |
| 30-34                                       | 360       | 28.21%          |
| 35-39                                       | 292       | 22.88%          |
| 40-49                                       | 277       | 21.71%          |
| 50-59                                       | 113       | 8.86%           |
| 60+                                         | 19        | 1.49%           |
| Unreported                                  | 0         | 0.00%           |
| **Total Team Members**                      | **1,276** | **100%**        |

**Of Note**: `Management` refers to Team Members who are *People Managers*, whereas `Leadership` denotes Team Members who are in *Director-level positions and above*.

**Source**: GitLab's HRIS, BambooHR
