---
layout: handbook-page-toc
title: "Customer Insight Page"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### Customer Insight Portraits
Customer Insight Portraits are continuously maintained profiles of customers (organizations, individuals, and/or communities) that enable and simplify high-impact content creation of **multiple types** while minimizing demand on customers and GitLab Team Members.

They are part of a renewed effort led by the [Customer Reference Program](https://about.gitlab.com/handbook/marketing/strategic-marketing/customer-reference-program/) to [Collaboratively](https://about.gitlab.com/handbook/values/#collaboration) generate superior [Results](https://about.gitlab.com/handbook/values/#results) in the most [Efficient](https://about.gitlab.com/handbook/values/#efficiency) manner possible.

#### (WIP) Customer Insight Portrait Process

The Customer Insight Portraits (CIP) will be Maintained under [Customer Reference Program Content - Customer Insight Portraits](https://gitlab.com/gitlab-com/marketing/strategic-marketing/customer-reference-content/customerinsightportraits) and be maintained by the Customer Reference Managers in their appropriate Regions as Parent Issues

Within the Parent issue is the customer snapshot, child issues, and links to active assets.  Each Parent Issue is labeled to easily find content within each CIP.

Order of Operations will be:

1. Customer Insight Portrait issue will be created

2. Customer Reference Manager begins to gather information (from Reference Edge, Command Plan, other internal resources such as sales call notes)

3. Customer Reference Manager syncing with TAM/AO

4. Customer Reference Manager conducts initial outreach call, links call notes and recording to Insight Portrait.

5. Customer Reference Manager identifies appropriate PMM and opens that issue, links to Insight Portrait and PMM Involvement Epic.

6. Customer Reference Manager provides customer with separate Metrics Google Form (see Value Drivers for details) and custom Customer Reference Questions determined in the PMM Issue, linking all content to Insight Portrait.
-  Customer Reference Manager creates support issues should customer need assistance in gathering metrics
-  Upload answers to Insight Portrait once received

7. Customer Reference Manager opens Partners issue, if applicable

8. Customer Reference Manager opens Content/Writers issue once PMM issue is closed 


- **Customer Insight Portrait Creation Process Initiated** 
  - Initiation vector noted (nomination, curation timer, event, etc.)
- **Initial, Internal Customer Insight Portrait Data Entered/Updated**
  - Data gathered/linked to from internal systems (data in SFDC, Gainsight, Demandbase, etc.)
  - Data gaps/needs and potential sources identified
- **Customer Insight Portrait Data Completion/Enrichment**
  - Data gathering mechanisms identified to complete profile (Surveys, Interviews - internal (w/Sales, Customer Success, etc.), external(w/customer, industry analyst, etc.), other research, etc.)
  - Plan created for performing data gathering tactics outlined w/DRIs and timing
  - Data gathering work performed
  - Initial Portrait reviewed with related stakeholders (Customer, Sales, Product Marketing, etc.) for quality and multiple narrative option identification for multiple output/artifact/content types (written case study, video, ebook, event speaking, media/press/analyst opportunities)
- **Activation (Leverage of Customer Insight Portraits)**
    - Notification of New and Refreshed Customer Insight Portraits to Stakeholders
      - Content and Case Study Teams and Process
      - Product Marketing
      - Customer Speaking Support Team and Process
      - more


### Customer Case Studies
Read the current GitLab customer case studies on the [GitLab customer page](https://about.gitlab.com/customers/).

#### Common Interview questions
Interview Questions: (Select the questions we should ask)

**Note, these questions will be adjusted based on specific UseCases and Value Drivers Identified by the CRM team, with possible custom additional questions as determined by PMM/CRM engagement, please confer with CRM before regarding appropriate timing for provision of questionnaire**

**Why GitLab**
- [ ] Describe what your organization does, how the software is used, and how your team helps solve business challenges.
- [ ] What problem were you trying to solve when you were looking at GitLab?
- [ ] What was your process before using GitLab?
- [ ] Why did you choose to replace your current tooling?
- [ ] What were the negative consequences of your previous environment?
- [ ] What would have happened if you hadn't replaced your tooling
- [ ] How was this problem affecting you?
- [ ] What products were you using before using GitLab?
- [ ] What were the required capabilities you were looking for?
- [ ] Why did you choose GitLab?
- [ ] Has GitLab helped you achieve the KPIs you set out to solve?
- [ ] Can you give me a bird's eye view of your toolchain? Which tools in your toolchain are the most important for the business? Why?
- [ ] What DevOps projects are you working on that are most important today, if you're able and comfortable speaking to them? Has that changed in the past year, if so, why do you think that is?
- [ ] Whats your biggest challenge re effective project delivery? (people/process/technology?) What's most disruptive to your day-to-day planned work?
- [ ] Is tech debt an issue that's affecting your development teams and ability to deliver, or has it been in the past?  If yes to either, how is it trending? (Has GitLab helped?)
- [ ] Which step in development takes the longest, and in which step are the most problems introduced?


**What if**
- [ ] What would have happened if you hadn't selected GitLab?

**Feedback on GitLab**
- [ ] How did GitLab solve those problems you were suffering from?
- [ ] Which teams are using GitLab?
- [ ] What do users say about GitLab?
- [ ] How has GitLab's monthly release cycle benefitted your SDLC?
- [ ] Has the regular release cycle supported innovation and collaboration?
- [ ] If your team is not on the most recent version (or within a release or two), why haven't you updated?

**Impact of GitLab**
- [ ] What are the positive business outcomes of introducing GitLab?
- [ ] What are some initial successes resulting from moving to GitLab?
- [ ] Can you share any larger successes?

### [Command of Message](https://about.gitlab.com/handbook/sales/command-of-the-message/) Questions

We have 3 Metric Form *templates* that serve as the basis for the custom Metric form we will send to customers along with their custom questions.  Based on the value drivers identified, combine forms together.

**Value Driver 1 - Increasing Operational Efficiencies**

[Value Driver 1 Metrics Form](https://forms.gle/BWd5JJn8zUaTTL1h7)

Steps:
- [ ] CRM to make a NEW COPY of this Template - Save in Customer Folder
- [ ] Customize with additional requisite questions based on PMM review/recommendation
- [ ] Send to Customer along with Customer Reference Questions prior to in-depth


**Value Driver 2 - Deliver Better Products Faster**

[Value Driver 2 Metrics Form](https://forms.gle/CRUtfGQyft7K17u36) 

Steps:
- [ ] CRM to make a NEW COPY of this Template - Save in Customer Folder
- [ ] Customize with additional requisite questions based on PMM review/recommendation
- [ ] Send to Customer along with Customer Reference Questions prior to in-depth


**Value Driver 3 - Reduce Security and Compliance Risk**

[Value Driver 3 Metrics Form](https://forms.gle/JZNeAWQnashwhSdDA)

Steps:
- [ ] CRM to make a NEW COPY of this Template - Save in Customer Folder
- [ ] Customize with additional requisite questions based on PMM review/recommendation
- [ ] Send to Customer along with Customer Reference Questions prior to in-depth

**Customer Use Case Questions**
Depending on the customers use cases; please select the appropriate questions below to ask the customer. 

**Version Control & Collaboration (VC&C)/Source Code Management (SCM)** 
- [ ] How does your team create, manage and protect your project artifacts. (source code, designs, configuration, etc)?
- [ ] How were you managing project assets prior to GitLab?
- [ ] Does having one SCM tool improve your development process (efficiency, speed, quality, etc)?

**Continuous Integration (CI)** 
- [ ] How are you automating software development processes, testing and building code?
- [ ] How does GitLab Continuous Integration add value to your SDLC?
- [ ] How has GitLab Continuous Integration helped improve quality and speed of your delivery? 
- [ ] How are you automating software development processes, testing and building code?
- [ ] What does your development workflow look like from initial code commit to shipping to production? Curious how you build and test code before it gets deployed, and if you're leveraging CI to automate your builds?
- [ ] What kinds of tests are you running today? Are they automated? Are you using any specific testing framework or integrating with any testing tools within your workflow?
- [ ] How does GitLab Continuous Integration add value to your SDLC?
- [ ] Since CI is a big part of achieving your goals, can you speak a little more about what success looks like for you on an individual and team level? What about at the organizational level?
- [ ] What are some positive outcomes or impacts that you've noticed since implementing GitLab for CI?
- [ ] How has GitLab Continuous Integration helped improve quality and speed of your delivery?
- [ ] Corresponds to Metrics Form: We find that the most meaningful case studies show value that changes over time with supporting quantitative data. These come in various forms of metrics and/or data points, such as DORA metrics (lead time, deployment frequency (more CD than CI), mean time to recovery, and change failure rate). What are you currently measuring today from a quantitative standpoint? If nothing right now, that's ok! Have you considered what you want to be able to track and measure in the short term?
- [ ] Corresponds to Metrics Form: Code commits: If you’re not already tracking the number of code commits your team(s) make before deploying your software, it’s a great time to take a look at that data in your environment. It should be fairly easy to retrieve, please let me know if you need guidance!



**Continuous Delivery (CD)**
- [ ] How are you improving the speed to deploy code?
- [ ] Has using GitLab Continuous Delivery empowered your developers to deploy changes faster?  What's their feedback?
- [ ] Has the automated delivery increased your code output while maintaining quality?

**Development, Security, and Operations (DevSecOps)** 
- [ ] How are you identifying application vulnerabilities during your development process?
- [ ] How has detecting security bugs earlier impacted the SDLC?
- [ ] How has GitLab helped support audits and compliance etc?
- [ ] How has shifting security earlier in the delivery lifecycle impacted your speed to deliver?

**Agile Project Management (Agile)** 
- [ ] How are you managing your team's backlog, sprints, milestones, and future work?
- [ ] How do you have visibility of team results?
- [ ] How do you track/monitor project milestones etc?

**Simplify Development Operations / End to End DevOps (DevOps)** 
- [ ] How has collaboration across teams improved with GitLab?
- [ ] How has productivity or efficiency improved using GitLab? 
- [ ] What other benefits have you realized with GitLab and a single application?

**Cloud Native Approach to Applications Development (CloudNative)**
- [ ] How are you developing and managing cloud native applications? 
> - [ ] K8s executor?
> - [ ] Canary deployments?
> - [ ] AutoDevOps? Using Templates?
> - [ ] CI/CD to orchestrate deployments?
- [ ] How has GitLab helped you to manage cloud native/microservices? 
- [ ] What are the benefits of using GitLab's kubernetes integration?
- [ ] How long have you been using Cloud Native, what has the journey been like?
> - [ ] When did you start adopting K8s? (before/after GitLab?)
- [ ] How many clusters do you have?
- [ ] If Autoscaling - what were you doing before?
> - [ ] How long did it take you? (cost of efficiency, speed, security)

**Infrastructure as Code (GitOps)**
- [ ] How are your operations teams managing different versions of their automation/configuration scripts?
- [ ] How are you testing and validating your infrastructure scripts?
- [ ] How are you integrating with other tools to maintain IAC?

**Deployment Strategy Questions**
- [ ] Can you describe the current deployment strategy you are using with GitLab? (target environment types, on-premise infrastructure and/or public cloud services, some combination of these and/or other resources)?
- [ ] Can you share the background and reasoning behind selecting this current deployment strategy?
- [ ] Were there specific business and/or technical reasons that shaped this strategy?
- [ ] Is your organization consuming or planning to consume cloud services? If so, which ones? and over what timeframe? Can you describe your current cloud deployment architecture-- if applicable? (If customer identified they are using cloud services or plan to)
- [ ] Are you using or planning to use containers? If so, can you describe your use of containers?

**Education Program Customers**
- [ ] How is GitLab advancing research at your University/Research Organization? 	
- [ ] What role does GitLab have in advancing open science?
- [ ] How are you using GitLab in your organization?
- [ ] What do the students and researchers like most about GitLab?
- [ ] What advantages does learning GitLab while in University give to students entering the professional world?
- [ ] How are students and researchers using GitLab to collaborate?

**Open Source Program Customers**
- [ ] How is GitLab impacting the number of contributions in your community?
- [ ] What kind of feedback have you received about GitLab from your community? 
- [ ] Did you need to adapt GitLab to fit your community’s needs? 
- [ ] What are the benefits of moving to GitLab for open source communities?
- [ ] What other services did you use before GitLab? (e.g. for hosting, task management, etc)
- [ ] What other services are you still using? (e.g. for hosting, task management, etc)
- [ ] Did you leverage any other open source programs? (e.g. Packet program, AWS credits, etc)
- [ ] (If not using the Community Edition) Which top tier enterprise features, if any, do you find most helpful? 
- [ ] How are you measuring the value that GitLab is delivering for your open source program?

### Written Case Study creation process

Written Case studies are vital to showcasing the success of our customers and display how they overcame the pain and challenges their organizations were facing in their software development lifecycle. This is an opportunity to describe how GitLab helps overcome these pain points and provide value to the organnization. Often these stories require time for proof points and metrics to be established within customer organization.  Here are the steps to creating written case studies. 

1. Sales Rep follows process outlined in [Reference Program Process](https://about.gitlab.com/handbook/marketing/strategic-marketing/customer-reference-program/#process-for-adding-new-reference-customers) to alert the Customer Reference Manager that customer has agreed to participating in written case study process. 
2. Sales Rep introduces CRM to customer by email. CRM coordinates an introduction call with customer.
3. CRM meets with customer for intro call to uncover customer story and to label case study issue with applicable value drivers and customer use case. CRM will share applicable [interview questions](https://about.gitlab.com/handbook/marketing/strategic-marketing/customer-reference-program/customer-insight/#common-interview-questions) with customer by email. During this session, CRM will work with customer to determine the best timeframe for interview call. Common time frames are 30 days, 60 days, 90 days or 6 months. CRM distributes the logo permission form to customer to help increase speed of legal approval. CRM will also bring up possibility of Media opportunities for case study. 
4. For the interview call, the CRM leads the discussion. The content creator is on the call to make sure questions are answered to get the case study completed and that they have everything they need to create case study. 
5. CRM gets interview transcribed using rev.com and sends the resulting transcript to customer for redlines/changes and approval to use. CRM to check with customer that legal and communications team is aligned with the process and is approving assets along the way. 
6. Once the transcript is approved for use, CRM puts approved transcript in the customer-identified folder in WIP Case Studes in Google Drive. CRM alerts Customer Content that the transcript is approved and links to it in the case study issue description. The customer transcript is stored in gdrive **only** under the named customer folder (it is not stored in rev.com)
7. Customer Content drafts case study and alerts CRM when draft is ready for review. 
8. CRM reviews draft for completeness, content and makes sure it acurately refects the customer story. CRM brings in designated use case PMM teammember for additional review (if necessary) and the Alliances/Partner team is alerted to draft by alert in the issue if the CRM deems it necessary. The additional reviewers will be tagged in the issue and also slacked the issue. They will have 3 working days to respond.The Draft is sent to PR team for media opportunity exploration. Draft is sent to content team for a copy edit review. 
9. Customer content uses feedback and creates a clean draft to send to customer. CRM sends draft to customer with Customer content cc'd in the email. This email should be sent as both a link to a google doc as well as a .docx file attached to the email. 
10. Once customer feedback is recieved, customer content addresses any concerns and CRM reviews and sends final version to customer for approval or continued discussion around draft. 
11. If the marketing approval form hasn't already been recieved, CRM sends the [marketing approval form](https://about.gitlab.com/handbook/marketing/strategic-marketing/customer-reference-program/#customer-logo-permission-form) for the customer to review, sign and return to GitLab.  Customer will also be requested to send a high res logo file (png/svg) to use on our website and marketing materials
12. CRM will review the PR potential of the reference with the PR team and if they deem it has PR merit, the CRM will propose PR support for the reference to the customer. The CRM will also propose social promotion of the case study [details here](#asking-the-customer-to-partner-with-us-on-social-promotion-of-the-published-case-study)
12. If customer agrees to media awareness around the case study, introduction to the PR team is co-ordinated by the CRM.
13. Once customer approves final version and a signed marketing permission form is recieved, CRM loads the signed form into the SFDC account and either customer content or CRM publishes case study on the /customers page. 
14. CRM and Customer content begin internal promotion process. Raise a Customer Content Activation issue, the CRM is the DRI for this activity. 

Please note that we can facilitate customer interviews in other languages (FR/DE/IT/ES/JP) if the customer prefers this option

#### Publishing to the website

1. Start by opening a public issue and an Merge Request in the www-gitlab-com project.
2. Create a `.yml` file in `/data/case_studies` directory under Marketing site repo (www-gitlab-com project). This can be accomplished in the Web IDE.
3. Keep the name of file same as company name (this is not mandatory but it is easier to manage), for eg; if company name is "Foobar", create a file as `/data/case_studies/foobar.yml`.
4.	Once created, add contents of the file using the sample Case Study template, or by scraping the content from an existing case study, and then update the values of each property based on case study details, remember, do NOT change property names.
5. Add images to the same MR. This ensures they show up in the preview app. To accomplish this, stay in your MR and on the left rail open the selected file. Upload the file to the specific directory by hovering over the file and selecting upload file. The Cover image needs to be a .jpg and put in 'source/image/blogimage' directory. The company logo needs to be a color .SVG image and is placed in the 'source/image/case_study_logos' directory. 
6.	In the Web IDE, you can view generated Case Study page in the generated App under the URL, for eg; http://localhost:4567/customers/foobar.
7. Once the case study is ready to publish, remove the WIP label from your MR and ask an approved publisher to post. 

#### Adding customer logo and case study to customer's grid

1. When a customer has approved the usage of their logo on our /customers page, we place their approved logo in the grid on the bottom of the page. Confirm that the customer industry categorisation is correct and aligns with their SFDC account.
2. Open an MR in the www-gitlab-com project. 
3. Add the approved single-tone .svg logo to the /source/images/organizations file.
4. Once the .svg is live in the organizations file, open the organizations.yml at www-gitlab-com in the /data file
5. Add new lines in the file to correspond to the exsiting format. 
6. Ensure the asset_type and asset_link are filled in and point to the live case study. 
7. Once the preview app shows a successful addition, remove the WIP label from your MR and ask an approved publisher to post. 

#### Asking the customer to partner with us on social promotion of the published case study

1. Once the customer agrees to social promotion of their case study; the CRM will create an issue [using the Social Media Case Study issue template](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/blob/master/.gitlab/issue_templates/social-case-study-promotion.md).
2. The CRM will request URLs for all of the customers social media pages (in order to tag the company pages),a high-quality resolution logo; one image for bright backgrounds and one image for dark background
3. Once the customer reverts with the materials; the CRM will update the issue and tag the Social Media Manager.

#### Localization process for Published Case References

1. Please follow the process for translating content for campaigns as detailed [here](https://about.gitlab.com/handbook/marketing/localization/#translating-content-for-campaigns) 
2. Please note the [prioritisation](https://about.gitlab.com/handbook/marketing/localization/#priority-countries) of content localization as per priority countries 
3. Once the customer case study content is localized by the external vendor; the requestor can raise an internal review [request](https://about.gitlab.com/handbook/marketing/localization/#process-for-requesting-a-review)




